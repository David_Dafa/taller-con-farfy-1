import socket
import json
import os


root_path ='/documentRoot'
base_path= os.path.dirname(os.path.abspath(__file__))+root_path
print("Servidor HTTP")
print(base_path)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 9100))
s.listen(1)

while True:
    client_connection, client_address = s.accept()
    request = client_connection.recv(1024).decode()

    request = request.replace('\r\n\r\n','')
    req_split = request.split('\r\n')
    #Extraer primera linea
    route = req_split[0].split(' ')
    method = route[0]
    protocol, version= route[2].split('/')
    route = route[1]
    if(route.endswith('/')):
    	route= route +'index.html'
    else: 
   		if not (route.endswith('.html') or route.endswith('/')):
   			route= route +'/index.html'



    print('route:'+ route + '\n----')
    print('method:'+ method + '\n----')
    print('protocol:'+ protocol + '\n----')
    print('version:'+ version + '\n----')
   
    print('split:--')
    req_split.pop(0)
    print(req_split)
    print('-----\n\n')
    #Extraer headers
    headers_map = {}
    for line in req_split :
    	head = line.split(': ')
    	headers_map[head[0]]=head[1]

    file= base_path+route
    print(file)
    json= 'X-RequestEcho: {"path": ' + '"'+route+'"' +" , " + '"protocol": ' + '"'+protocol+"/"+version+" , "+'"method" :'+'"'+method+'"'+" , "+'"headers" :' 
    print(json,headers_map)
    if os.path.exists(file) : 
    	client_connection.sendall("HTTP/1.1 200 OK".encode())
    	send = open(file,'r')
    	for line in send:
    		client_connection.sendall(line.encode())
    	send.close()

    else:
    	client_connection.sendall("HTTP/1.1 404 Not Found".encode())

    
    client_connection.close()